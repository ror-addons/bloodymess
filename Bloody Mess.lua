--[[
Bloody Mess
Warhammer Online: Age of Reckoning UI modification that adds in
blood effects when you hit things or get hit by things.
    
Copyright (C) 2008  Dillon "Rhekua" DeLoss
rhekua@msn.com		    www.rhekua.com

THE FOLLOWING DOES NOT APPLY TO THE IMAGES SUPPLIED WITH THIS
MODIFICATION. ALL RIGHTS ARE RESERVED FOR SAID IMAGES AND THEY
MAY NOT BE USED OUTSIDE THIS MODIFICATION FOR ANY PURPOSE 
WITHOUT EXPRESSED WRITTEN CONSENT FROM DILLON "RHEKUA" DELOSS.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]--

local TIME_DELAY = 0.033333333 -- 30fps
local timeLeft = TIME_DELAY

BloodyMess = {}

BloodyMess.enabled = true
BloodyMess.amountModifier = 1
BloodyMess.amountNormal = 1
BloodyMess.amountCritical = 2

BloodyMess.effects = {
					-- sprays
					"bm_sp_1",
					"bm_sp_2",
					"bm_sp_3",
					"bm_sp_4",
					"bm_sp_5",
					"bm_sp_6",
					"bm_sp_7",
					"bm_sp_8",
					"bm_sp_9",
					"bm_sp_10",

					-- swipes
					"bm_sw_1",
					"bm_sw_2",
					"bm_sw_3",
					"bm_sw_4",
					"bm_sw_5",
				}

BloodyMess.windows = {}

function BloodyMess.OnInitialize()
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "BloodyMess.OnWorldObjCombatEvent")

	LabelSetText("BloodyMessOptionsTitleBarTextLabel", L"Bloody Mess")
	LabelSetText("BloodyMessOptionsSliderBar1Text", L"Overall Blood Multiplier")
	SliderBarSetCurrentPosition("BloodyMessOptionsSliderBar1", (BloodyMess.amountModifier-1)/2)
	LabelSetText("BloodyMessOptionsSliderBar2Text", L"Blood Amount - Normal")
	SliderBarSetCurrentPosition("BloodyMessOptionsSliderBar2", (BloodyMess.amountNormal-1)/4)
	LabelSetText("BloodyMessOptionsSliderBar3Text", L"Blood Amount - Critical")
	SliderBarSetCurrentPosition("BloodyMessOptionsSliderBar3", (BloodyMess.amountCritical-1)/9)
	LabelSetText( "BloodyMessOptionsEnableCheckBoxLabel", L"Enabled")
	ButtonSetStayDownFlag( "BloodyMessOptionsEnableCheckBoxButton", true )
	ButtonSetPressedFlag( "BloodyMessOptionsEnableCheckBoxButton", BloodyMess.enabled )
	ButtonSetText("BloodyMessOptionsApplyButton", L"Apply")
end

function BloodyMess.ApplyButton_OnLButtonUp()
	BloodyMess.amountModifier = math.floor((SliderBarGetCurrentPosition("BloodyMessOptionsSliderBar1")*2)+0.5)+1
	BloodyMess.amountNormal = math.floor((SliderBarGetCurrentPosition("BloodyMessOptionsSliderBar2")*4)+0.5)+1
	BloodyMess.amountCritical = math.floor((SliderBarGetCurrentPosition("BloodyMessOptionsSliderBar3")*9)+0.5)+1
	BloodyMess.enabled = ButtonGetPressedFlag( "BloodyMessOptionsEnableCheckBoxButton" )
	for k, v in pairs(BloodyMess.windows) do
		WindowSetShowing(k, false)
	end
end

function BloodyMess.GetResolutionScale()
-- i made the mod for 1920x1200, so it must be scaled properly for other resolutions
	return ((SystemData.screenResolution.x/1920)+(SystemData.screenResolution.y/1200))/2
end

function BloodyMess.PlayEffect( worldObjNum, bloodEffect, grow )
	local numWindows = 0

	if ( bloodEffect == nil ) then
		bloodEffect = BloodyMess.effects[math.random(table.getn(BloodyMess.effects))]
	end

	if ( grow == nil ) then
		if ( math.floor(math.random()+0.5) == 1 ) then
			grow = true
		else
			grow = false
		end
	end

	local minSpeed = 80
	local maxSpeed = 350
	if ( string.find(bloodEffect, "sw", 1, true) ~= nil ) then
	-- this ensures that "swipe" effects fly across the screen fast
		minSpeed = 500
		maxSpeed = 1000
	end
	
	for k, v in pairs(BloodyMess.windows) do
		if ( WindowGetShowing(k) == false ) then
		-- find a free window
			WindowSetShowing(k, true)
			WindowSetScale(k, BloodyMess.GetResolutionScale())
			WindowSetAlpha(k, 0)
			DynamicImageSetTexture(k, bloodEffect, 0, 0)
			BloodyMess.SetRotation( k, math.random(360) )
			BloodyMess.SetPosition( k, 0, 200 )
			BloodyMess.windows[k].worldObjNum = worldObjNum
			BloodyMess.windows[k].scale.min = 0.25+math.random()*(BloodyMess.GetResolutionScale()/2)
			BloodyMess.windows[k].scale.grow = grow
			BloodyMess.windows[k].speed = minSpeed+math.random(maxSpeed-minSpeed)/(BloodyMess.windows[k].scale.min+0.5)
			BloodyMess.windows[k].lifetime = 0.5+math.random()*0.5
			BloodyMess.windows[k].time = BloodyMess.windows[k].lifetime
			return
		end
		numWindows = numWindows + 1
	end

	-- couldn't find a free window, time to make one!

	local windowName = "bm_e_" .. numWindows
	CreateWindowFromTemplate(windowName, "bm_blood_effect", "Root")
	WindowSetShowing(windowName, true)
	WindowSetAlpha(windowName, 0)
	DynamicImageSetTexture(windowName, bloodEffect, 0, 0)
	local rot = math.random(360)
	local minScale = math.random()*0.5
	local lifetime = 0.5+math.random()*0.5
	BloodyMess.windows[windowName] = { 
							rotation = rot,
							direction = { 
										x = math.cos(math.rad(rot)), 
										y = math.sin(math.rad(rot)),
									},
							position = { 
										x = 0, 
										y = 200,
									},
							scale = { 
										min = minScale,
										max = BloodyMess.GetResolutionScale(),
										grow = false,
									},
							speed = minSpeed+math.random(maxSpeed-minSpeed)/(minScale+0.5),
							worldObjNum = worldObjNum,
							lifetime = lifetime,
							time = lifetime,
						 }

end

function BloodyMess.TEST()
	if ( not BloodyMess.enabled ) then
		EA_ChatWindow.Print(L"Bloody Mess is DISABLED!")
		return
	end
	BloodyMess.PlayEffect( GameData.Player.worldObjNum )
end

function BloodyMess.GetPosition( windowName )
	if ( BloodyMess.windows[windowName] ~= nil ) then

		return BloodyMess.windows[windowName].position.x, BloodyMess.windows[windowName].position.y
	end
end

function BloodyMess.SetPosition( windowName, x, y )
	if ( BloodyMess.windows[windowName] ~= nil 
		and type(x) == "number" and type(y) == "number" ) then

		BloodyMess.windows[windowName].position.x = x
		BloodyMess.windows[windowName].position.y = y
	end
end

function BloodyMess.SetRotation( windowName, rotation )
	if ( BloodyMess.windows[windowName] ~= nil 
		and type(rotation) == "number" ) then

		BloodyMess.windows[windowName].rotation = rotation
		BloodyMess.windows[windowName].direction.x = math.cos(math.rad(rotation))
		BloodyMess.windows[windowName].direction.y = math.sin(math.rad(rotation))
	end
end

function BloodyMess.OnUpdate( elapsed )

	timeLeft = timeLeft - elapsed
	if timeLeft > 0 then
		return -- cut out early
	end
	timeLeft = TIME_DELAY -- reset to TIME_DELAY seconds
	-- this will run roughly every second. If TIME_DELAY were 2, it'd run every 2 seconds.

	WindowSetShowing("BloodyMessOptions", WindowGetShowing("SettingsWindowTabbed"))

	if ( not BloodyMess.enabled ) then
		return
	end

	for k, v in pairs(BloodyMess.windows) do
		if ( BloodyMess.windows[k].time > 0 ) then
			BloodyMess.windows[k].time = BloodyMess.windows[k].time-TIME_DELAY

			MoveWindowToWorldObject(k, BloodyMess.windows[k].worldObjNum, 1.0)
			
			--DynamicImageSetRotation(k, math.deg(math.atan2(v.direction.y,v.direction.x)))
			DynamicImageSetRotation(k, v.rotation)

			local alpha = BloodyMess.windows[k].time/BloodyMess.windows[k].lifetime

			local x, y = BloodyMess.GetPosition( k )
			x = x + (v.direction.x * v.speed * TIME_DELAY) * alpha * BloodyMess.GetResolutionScale()
			y = y + (v.direction.y * v.speed * TIME_DELAY) * alpha * BloodyMess.GetResolutionScale()
			BloodyMess.SetPosition( k, x, y)

			local x2, y2 = WindowGetOffsetFromParent(k)
			WindowSetMoving(k, true)
			WindowSetOffsetFromParent(k, x+x2, y+y2)
			WindowSetMoving(k, false)

			local sX, sY = WindowGetScreenPosition(k)
			BloodyMess.windows[k].scale.max = 0.5 + ((sY/SystemData.screenResolution.y) * BloodyMess.GetResolutionScale())

			local scale = v.scale.max + (math.abs(v.scale.max - v.scale.min) * (alpha-1))
			if ( v.scale.grow ) then
				scale = v.scale.min - (math.abs(v.scale.max - v.scale.min) * (alpha-1))
			end

			WindowSetScale(k, scale)

			if ( alpha > 0 ) then
				WindowSetAlpha(k, alpha)
			else
				WindowSetShowing(k, false)
			end

			--[[local x, y = WindowGetOffsetFromParent(k)
			WindowSetMoving(k, true)
			WindowSetOffsetFromParent(k, x + (v.direction.x * v.speed * TIME_DELAY), y + (v.direction.y * v.speed * TIME_DELAY))
			WindowSetMoving(k, false)]]--
		end
	end
end

function BloodyMess.OnWorldObjCombatEvent( hitTargetObjectNumber, hitAmount, textType )
	if ( not BloodyMess.enabled ) then
		return
	end

	if ( hitAmount < 0 ) then
		if ( textType == 0 ) then
			-- normal melee damage
			for i = 1, BloodyMess.amountNormal*BloodyMess.amountModifier do
				BloodyMess.PlayEffect( hitTargetObjectNumber, BloodyMess.effects[math.random(table.getn(BloodyMess.effects)-5)] )
			end
		elseif ( textType == 1 ) then
			-- normal magic damage
			for i = 1, BloodyMess.amountNormal*BloodyMess.amountModifier do
				BloodyMess.PlayEffect( hitTargetObjectNumber, BloodyMess.effects[math.random(table.getn(BloodyMess.effects)-5)] )
			end
		elseif ( textType == 2 ) then
			-- critical melee damage
			for i = 1, (BloodyMess.amountNormal+BloodyMess.amountCritical)*BloodyMess.amountModifier do
				BloodyMess.PlayEffect( hitTargetObjectNumber, nil, true )
			end
		elseif ( textType == 3 ) then
			-- critical magic damage
			for i = 1, (BloodyMess.amountNormal+BloodyMess.amountCritical)*BloodyMess.amountModifier do
				BloodyMess.PlayEffect( hitTargetObjectNumber, BloodyMess.effects[math.random(table.getn(BloodyMess.effects)-5)], true )
			end
		end
	end
end

function BloodyMess.OnShutdown()
	UnregisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "BloodyMess.OnWorldObjCombatEvent")

	for k, v in pairs(BloodyMess.windows) do
		DestroyWindow(k)
	end

	BloodyMess.windows = {}
end