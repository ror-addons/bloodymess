<?xml version="1.0" encoding="UTF-8"?>
<!--
Bloody Mess
Warhammer Online: Age of Reckoning UI modification that adds in
blood effects when you hit things or get hit by things.
    
Copyright (C) 2008  Dillon "Rhekua" DeLoss
rhekua@msn.com		    www.rhekua.com

THE FOLLOWING DOES NOT APPLY TO THE IMAGES SUPPLIED WITH THIS
MODIFICATION. ALL RIGHTS ARE RESERVED FOR SAID IMAGES AND THEY
MAY NOT BE USED OUTSIDE THIS MODIFICATION FOR ANY PURPOSE 
WITHOUT EXPRESSED WRITTEN CONSENT FROM DILLON "RHEKUA" DELOSS.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Bloody Mess" version="1.2" date="3/2/2009" >
		<Author name="Rhekua" email="rhekua@msn.com" />
		<Description text="Blood effects for when you hit things or get hit by things!" />
		<Files>
			<File name="Bloody Mess.lua" />
			<File name="Bloody Mess.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="BloodyMess.enabled" />
			<SavedVariable name="BloodyMess.amountModifier" />
			<SavedVariable name="BloodyMess.amountNormal" />
			<SavedVariable name="BloodyMess.amountCritical" />
		</SavedVariables>
		<OnInitialize>
      		<CreateWindow name="BloodyMessOptions" show="false" />
			<CallFunction name="BloodyMess.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="BloodyMess.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="BloodyMess.OnShutdown" />
    		</OnShutdown>
	</UiMod>
</ModuleFile>
